// Using DOM
// Retrieve an element from webpage
const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

/* Alternative in retrieving an element - getElement
	document.getElementById('text-first-name');
	document.getElementsByClassName('txt-inputs');
	document.getElementsByTagName('input');
*/

// interaction = event (click, keypress..)
// Event listener - an interaction b/w the user and the webpage

txtFirstName.addEventListener ('keyup', (event) => {
spanFullName.innerHTML = txtFirstName.value;
})

// //Sorry sir, I cant remember how to do callback fn :( I cant even understand what google says, i tried a tutorial but I dont get it :(

txtLastName.addEventListener ('keyup', (event) => {
spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
})


// Assign same event to multiple listeners

txtFirstName.addEventListener('keyup', (event) => {
	// Contains the element where the evemt happened
	console.log(event.target);
	// Gets the value of the input object
	console.log(event.target.value);
})

txtLastName.addEventListener('keyup', (event) => {
	// Contains the element where the evemt happened
	console.log(event.target);
	// Gets the value of the input object
	console.log(event.target.value);
})


